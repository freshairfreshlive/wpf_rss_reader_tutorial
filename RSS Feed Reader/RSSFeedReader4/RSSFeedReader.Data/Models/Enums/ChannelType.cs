﻿namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Enum of the possible Channel types.
    /// </summary>
    public enum ChannelType
    {
        /// <summary>
        /// Channel is a feed.
        /// </summary>
        Feed,

        /// <summary>
        /// Channel is a folder.
        /// </summary>
        Folder,

        /// <summary>
        /// ChannelType has not been assigned.
        /// </summary>
        None,
    }
}
