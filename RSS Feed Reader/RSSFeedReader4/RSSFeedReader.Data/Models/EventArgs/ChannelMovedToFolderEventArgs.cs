﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelMovedToFolder"/> event.
    /// </summary>
    public class ChannelMovedToFolderEventArgs : EventArgs
    {
        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelMovedToFolderEventArgs"/> class.
        /// </summary>
        /// <param name="channelToMove">The <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        public ChannelMovedToFolderEventArgs(Channel channelToMove)
            : this(channelToMove, null)
        {
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelMovedToFolderEventArgs"/> class.
        /// </summary>
        /// <param name="channelToMove">The <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        /// <param name="destinationFolder">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// folder the channel is being moved to.</param>
        public ChannelMovedToFolderEventArgs(Channel channelToMove, Channel destinationFolder)
            : this(channelToMove, destinationFolder, null)
        {           
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelMovedToFolderEventArgs"/> class.
        /// </summary>
        /// <param name="channelToMove">The <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        /// <param name="destinationFolder">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// folder the channel is being moved to.</param>
        /// <param name="parent">The parent of the <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        public ChannelMovedToFolderEventArgs(Channel channelToMove, Channel destinationFolder, Channel parent )
        {
            Parent = parent;
            DestinationFolder = destinationFolder;
            ChannelToMove = channelToMove;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the folder the <see cref="RSSFeedReader.Data.Models.Channel"/> is being moved to.
        /// </summary>
        public Channel DestinationFolder { get; private set; }

        /// <summary>
        /// Gets the parent of this <see cref="RSSFeedReader.Data.Models.Channel"/>.
        /// </summary>
        public Channel Parent { get; private set; }

        /// <summary>
        /// Gets the <see cref="RSSFeedReader.Data.Models.Channel"/> to move.
        /// </summary>
        public Channel ChannelToMove { get; private set; }
        #endregion
    }
}
