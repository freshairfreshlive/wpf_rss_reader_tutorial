﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for ManageFeedsView.xaml
    /// </summary>
    public partial class ManageFeedsView : Window
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Views.ManageFeedsView"/> class.
        /// </summary>
        public ManageFeedsView()
        {
            InitializeComponent();
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
