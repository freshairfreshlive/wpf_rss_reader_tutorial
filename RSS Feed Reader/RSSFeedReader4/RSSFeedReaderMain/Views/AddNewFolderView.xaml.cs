﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for AddNewFolder.xaml
    /// </summary>
    public partial class AddNewFolderView : Window
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="AddNewFolderView"/> class.
        /// </summary>
        public AddNewFolderView()
        {
            InitializeComponent();
            _NameTextBox.Focus();
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
