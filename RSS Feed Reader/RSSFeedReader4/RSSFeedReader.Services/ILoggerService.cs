﻿
namespace RSSFeedReader.Services
{
    /// <summary>
    /// This interface defines a very simple logger interface
    /// to allow the ViewModel to log.
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        /// Logs to the event log using the params provided.
        /// </summary>
        /// <param name="type">The type of logging.</param>
        /// <param name="message">The actual message string to be logged.</param>
        void LogMessage( string type, string message );
    }
}
