﻿using System.Windows.Controls;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for ChannelView.xaml
    /// </summary>
    public partial class ChannelView : UserControl
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="ChannelView"/> class.
        /// </summary>
        public ChannelView()
        {
            InitializeComponent();
        }
    }
}
