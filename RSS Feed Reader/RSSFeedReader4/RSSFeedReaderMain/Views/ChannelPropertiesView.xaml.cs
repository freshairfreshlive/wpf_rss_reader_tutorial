﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for ChannelPropertiesView.xaml
    /// </summary>
    public partial class ChannelPropertiesView : Window
    {
        /// <summary>
        /// Initialize a new instace of the <see cref="RSSFeedReader.Views.ChannelPropertiesView"/> class.
        /// </summary>
        public ChannelPropertiesView()
        {
            InitializeComponent();
            nameTB.Focus();
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
