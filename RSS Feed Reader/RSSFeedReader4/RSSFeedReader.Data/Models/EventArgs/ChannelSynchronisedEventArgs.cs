﻿using System;
using System.ComponentModel;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelSynchronised"/> event.
    /// </summary>
    public class ChannelSynchronisedEventArgs : AsyncCompletedEventArgs
    {
        #region Fields
        private Channel _channelSynchronised; 
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelSynchronisedEventArgs"/> class.
        /// for successful completion.
        /// </summary>
        /// <param name="channelSynchronised">
        /// The <see cref="RSSFeedReader.Data.Models.Channel"/> instance being synched.
        /// </param>
        /// <param name="userState">
        /// The optional user-supplied state object passed to the 
        /// System.ComponentModel.BackgroundWorker.RunWorkerAsync(System.Object) method.
        /// </param>
        public ChannelSynchronisedEventArgs(Channel channelSynchronised, object userState)
            : base(null, false, userState)
        {
            _channelSynchronised = channelSynchronised;
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelSynchronisedEventArgs"/> class
        /// for an error or a cancellation.
        /// </summary>
        /// <param name="error">
        /// An error that occured during the asynchronous operation.
        /// </param>
        /// <param name="cancelled">
        /// A value indicating whether the asynchronous operation was cancelled.
        /// </param>
        /// <param name="userState">
        /// The optional user-supplied state object passed to the 
        /// System.ComponentModel.BackgroundWorker.RunWorkerAsync(System.Object) method.
        /// </param>
        public ChannelSynchronisedEventArgs(Exception error, bool cancelled, object userState)
            : base(error, cancelled, userState)
        {
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the <see cref="RSSFeedReader.Data.Models.Channel"/> instance being synched.
        /// </summary>
        public Channel SynchronisedChannel
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return _channelSynchronised;
            }
        }

        #endregion
    }
}
