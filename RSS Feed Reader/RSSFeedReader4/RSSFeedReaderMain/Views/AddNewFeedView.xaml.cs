﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for AddNewFeedView.xaml
    /// </summary>
    public partial class AddNewFeedView : Window
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="AddNewFeedView"/> class.
        /// </summary>
        public AddNewFeedView()
        {
            InitializeComponent();
            _feedUrlTextBox.Focus();
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
