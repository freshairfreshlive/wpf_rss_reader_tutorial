﻿using System;
using GalaSoft.MvvmLight.Command;
using RSSFeedReader.Resources;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// Class to rename a <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/>.
    /// </summary>
    public class RenameChannelViewModel : MyViewModelBase
    {
        #region Fields
        readonly ChannelViewModel _channelViewModel;
        #endregion

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.RenameChannelViewModel"/> class.
        /// </summary>
        public RenameChannelViewModel() { }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.RenameChannelViewModel"/> class
        /// with a reference to a <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/> instance.
        /// </summary>
        /// <param name="channelViewModel">The <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/>
        /// instance to use as the data provider.</param>
        public RenameChannelViewModel(ChannelViewModel channelViewModel)
        {
            if (channelViewModel == null)
                throw new ArgumentNullException("channelViewModel");

            _channelViewModel = channelViewModel;
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the name of the folder.
        /// </summary>
        public string Title
        {
            get { return _channelViewModel.Title; }
        }

        #region OKCommand
        private RelayCommand<string> _okCommand;

        /// <summary>
        /// Adds a new folder.
        /// </summary>
        public RelayCommand<string> OKCommand
        {
            get
            {
                return _okCommand ?? (_okCommand = new RelayCommand<string>(title =>
                    {
                        try
                        {
                            _channelViewModel.Title = title;
                        }
                        catch (Exception ex)
                        {
                            Logger.LogMessage(Strings.LogTypeWarning, string.Format("An error has occured: {0}", ex.Message));
                        }
                        finally
                        {
                            CloseView();
                        }
                    },
                    title => !string.IsNullOrEmpty(title)));
            }
        }
        #endregion
        #endregion
    }
}
