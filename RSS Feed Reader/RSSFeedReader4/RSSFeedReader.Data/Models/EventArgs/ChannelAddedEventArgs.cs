﻿using System;
using System.ComponentModel;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelAdded"/> event.
    /// </summary>
    public class ChannelAddedEventArgs : AsyncCompletedEventArgs
    {
        #region Fields
        private Channel _newChannel;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelAddedEventArgs"/> class.
        /// for successful completion.
        /// </summary>
        /// <param name="newChannel">
        /// The new <see cref="RSSFeedReader.Data.Models.Channel"/> instance being added.
        /// </param>
        /// <param name="userState">
        /// The optional user-supplied state object passed to the 
        /// System.ComponentModel.BackgroundWorker.RunWorkerAsync(System.Object) method.
        /// </param>
        public ChannelAddedEventArgs(Channel newChannel, object userState)
            : base(null, false, userState)
        {
            _newChannel = newChannel;
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelAddedEventArgs"/> class
        /// for an error or a cancellation.
        /// </summary>
        /// <param name="error">
        /// An error that occured during the asynchronous operation.
        /// </param>
        /// <param name="cancelled">
        /// A value indicating whether the asynchronous operation was cancelled.
        /// </param>
        /// <param name="userState">
        /// The optional user-supplied state object passed to the 
        /// System.ComponentModel.BackgroundWorker.RunWorkerAsync(System.Object) method.
        /// </param>
        public ChannelAddedEventArgs(Exception error, bool cancelled, object userState)
            : base(error, cancelled, userState)
        {
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the new <see cref="RSSFeedReader.Data.Models.Channel"/> instance.
        /// </summary>
        public Channel NewChannel
        {
            get
            {
                base.RaiseExceptionIfNecessary();
                return _newChannel;
            }
        }

        #endregion
    }
}
