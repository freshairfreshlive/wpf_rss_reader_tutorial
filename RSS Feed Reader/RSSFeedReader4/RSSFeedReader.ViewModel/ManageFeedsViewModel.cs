﻿using System;
using Microsoft.Practices.ServiceLocation;
using RSSFeedReader.Data.Models;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// View Model only exposes the options
    /// for the application.
    /// </summary>
    public class ManageFeedsViewModel : ManageChannelBaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialize a new instance of the OptionsViewModel class.
        /// </summary>
        public ManageFeedsViewModel()
        {
            RefreshPeriod = ChannelDataSource.Instance.RefreshPeriod;
            Address = ChannelDataSource.Instance.ProxySetting.Address;
            Port = ChannelDataSource.Instance.ProxySetting.Port;
            UserName = ChannelDataSource.Instance.ProxySetting.UserName;
            Password = ChannelDataSource.Instance.ProxySetting.Password;
            Domain = ChannelDataSource.Instance.ProxySetting.Domain;
            IsProxyServerEnabled = ChannelDataSource.Instance.ProxySetting.IsProxyServerEnabled;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the refresh period for the application.
        /// </summary>
        public int RefreshPeriod { get; set; }

        /// <summary>
        ///The URI of the proxy server.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The port number on host to use.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets if the proxy server is enabled.
        /// </summary>
        public bool IsProxyServerEnabled { get; set; }
        #endregion

        protected override void CloseView()
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().Timer.Interval = TimeSpan.FromMinutes(RefreshPeriod);
            ChannelDataSource.Instance.RefreshPeriod = RefreshPeriod;
            ChannelDataSource.Instance.ProxySetting.Address = Address;
            ChannelDataSource.Instance.ProxySetting.Port = Port;
            ChannelDataSource.Instance.ProxySetting.Domain = Domain;
            ChannelDataSource.Instance.ProxySetting.Password = Password;
            ChannelDataSource.Instance.ProxySetting.UserName = UserName;
            ChannelDataSource.Instance.ProxySetting.IsProxyServerEnabled = IsProxyServerEnabled;
            base.CloseView();
        }
    }
}
