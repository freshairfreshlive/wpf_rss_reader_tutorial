﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Base class for all RSS feed classes.
    /// </summary>
    [Serializable]
    public abstract class FeedBase
    {
        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.FeedBase"/> class.
        /// </summary>
        public FeedBase()
        {
        }

        /// <summary>
        /// Initialze a new instance of the <see cref="RSSFeedReader.Data.Models.FeedBase"/> class.
        /// </summary>
        /// <param name="title">The title of the feed.</param>
        /// <param name="description">The description of the feed.</param>
        /// <param name="link">The link to the feed.</param>
        public FeedBase(string title, string description, string link)
        {
            Title = title;
            Description = description;
            Link = link;
        } 

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the title of the feed.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the description of the feed.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets link to the feed.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Returns the title of the feed.
        /// </summary>
        /// <returns>The Title of the feed.</returns>
        public override string ToString()
        {
            return Title;
        } 

        #endregion
    }
}
