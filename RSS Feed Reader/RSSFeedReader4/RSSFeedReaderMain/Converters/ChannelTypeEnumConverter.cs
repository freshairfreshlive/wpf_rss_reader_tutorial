﻿using System;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using RSSFeedReader.Data.Models;

namespace RSSFeedReader.Converters
{
    /// <summary>
    /// Chooses the correct image to display based on the <see cref="RSSFeedReader.Data.Models.ChannelType"/>.
    /// </summary>
    public class ChannelTypeEnumConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        /// <remarks>If the ChannelType is ChannelType.Feed then return a RSS channel icon,
        /// otherwise returns a folder icon.</remarks>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ChannelType cType = (ChannelType)value;

            switch (cType)
            {
                case ChannelType.Feed:
                    return new BitmapImage(new Uri("pack://application:,,,/RSSFeedReader.Resources;component/Images/Newsfeed.png"));
                case ChannelType.Folder:
                    return new BitmapImage(new Uri("pack://application:,,,/RSSFeedReader.Resources;component/Images/CloseFolder.png"));
                default:
                    return null;
            }
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>A converted value. If the method returns null, the valid null value is used.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}