﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelRemoved"/> event.
    /// </summary>
    public class ChannelRemovedEventArgs : EventArgs
    {
        #region Constructor
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelRemovedEventArgs"/> class.
        /// </summary>
        /// <param name="oldChannel">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// instance being removed.</param>
        public ChannelRemovedEventArgs(Channel oldChannel)
        {
            OldChannel = oldChannel;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the old <see cref="RSSFeedReader.Data.Models.Channel"/> instance.
        /// </summary>
        public Channel OldChannel { get; private set; }
        #endregion
    }
}
