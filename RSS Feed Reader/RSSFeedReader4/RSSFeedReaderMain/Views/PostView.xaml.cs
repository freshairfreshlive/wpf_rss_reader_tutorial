﻿using System.Windows.Controls;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for PostView.xaml
    /// </summary>
    public partial class PostView : UserControl
    {
        /// <summary>
        /// Initialize a new instance of the PostView class.
        /// </summary>
        public PostView()
        {
            InitializeComponent();
        }
    }
}
