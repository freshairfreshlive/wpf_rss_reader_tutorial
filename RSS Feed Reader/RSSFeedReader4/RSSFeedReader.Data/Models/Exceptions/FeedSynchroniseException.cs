﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Represents errors that occur whilst trying to sync a feed.
    /// </summary>
    public class FeedSynchroniseException : Exception
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedSynchroniseException"/> class with a specified
        ///  error message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public FeedSynchroniseException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedSynchroniseException"/> class with a specified
        /// error message and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.
        /// </param>
        public FeedSynchroniseException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedSynchroniseException"/> class with a specified
        /// error message, the url that failed to load
        /// and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="url">
        /// The url of the feed that failed to sync.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.
        /// </param>
        public FeedSynchroniseException(string message, string url, Exception innerException)
            : base(message, innerException)
        {
            ChannelUrl = url;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedSynchroniseException"/> class with a specified
        /// error message, the url that failed to load
        /// and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="url">
        /// The url of the feed that failed to sync.
        /// </param>
        /// <param name="id">
        /// The ID of the feed channel being synchronised.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.
        /// </param>
        public FeedSynchroniseException(string message, string url, Guid id, Exception innerException)
            : base(message, innerException)
        {
            ChannelUrl = url;
            ChannelID = id;
        }

        /// <summary>
        /// Gets the url that failed to sync.
        /// </summary>
        public string ChannelUrl { get; private set; }

        /// <summary>
        /// Gets the ID of the feed Channel that failed to sync.
        /// </summary>
        public Guid ChannelID { get; private set; }
    }
}
