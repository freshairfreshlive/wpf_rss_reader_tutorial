﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RSSFeedReader.Data.Models;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// View Model only exposes a subset of the underlying
    /// Post model properties.
    /// </summary>
    public class PostViewModel :MyViewModelBase
    {
        #region Fields
        readonly Post _post;
        bool _isSelected;

        #endregion

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.PostViewModel"/> class.
        /// </summary>
        public PostViewModel()
        {
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.PostViewModel"/> class.
        /// </summary>
        /// <param name="post">
        /// The <see cref="RSSFeedReader.Data.Models.Post"/> that 
        /// this <see cref="RSSFeedReader.ViewModels.PostViewModel"/> instance relates to.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Null argument passed in.
        /// </exception>
        public PostViewModel(Post post)
        {
            if (post == null)
                throw new ArgumentNullException("post");

            _post = post;
        } 

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the title of the post.
        /// </summary>
        public string Title
        {
            get { return _post.Title; }
        }

        /// <summary>
        /// Gets the summary of the post.
        /// </summary>
        public string Description
        {
            get { return _post.Description; }
        }

        /// <summary>
        /// Gets the link to the post.
        /// </summary>
        public string Link
        {
            get { return _post.Link; }
        }

        /// <summary>
        /// Gets the date the post was published.
        /// </summary>
        public string PublishedDate 
        { 
            get { return _post.PublishedDate; } 
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="RSSFeedReader.ViewModels.PostViewModel"/>
        /// instance is selected in the UI.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == _isSelected)
                    return;

                if (value)
                    IsRead = true;

                _isSelected = value;
                base.RaisePropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="RSSFeedReader.ViewModels.PostViewModel"/> 
        /// instance has been read or not.
        /// </summary>
        public bool IsRead
        {
            get { return _post.IsRead; }
            set
            {
                if (value == _post.IsRead)
                    return;

                _post.IsRead = value;
                base.RaisePropertyChanged("IsRead");
            }
        }

        /// <summary>
        /// Returns a System.String that represents the current <see cref="RSSFeedReader.ViewModels.PostViewModel"/>.
        /// </summary>
        /// <returns>
        /// The title of this <see cref="RSSFeedReader.ViewModels.PostViewModel"/>.
        /// </returns>
        public override string ToString()
        {
            return Title;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a new ObservableCollection of type <see cref="RSSFeedReader.ViewModels.PostViewModel"/>
        /// from a List of type <see cref="RSSFeedReader.Data.Models.Post"/>.
        /// </summary>
        /// <param name="posts">
        /// The List of type <see cref="RSSFeedReader.Data.Models.Post"/> to create the
        /// ObservableCollection of type <see cref="RSSFeedReader.ViewModels.PostViewModel"/> from.
        /// </param>
        /// <returns>
        /// An ObservableCollection of type <see cref="RSSFeedReader.ViewModels.PostViewModel"/>.
        /// </returns>
        public static ObservableCollection<PostViewModel> CreatePostViewModel(List<Post> posts)
        {
            ObservableCollection<PostViewModel> postsVM;
            postsVM = new ObservableCollection<PostViewModel>(
                (from post in posts
                 select new PostViewModel(post)).ToList());

            return postsVM;
        } 

        #endregion
    }
}
