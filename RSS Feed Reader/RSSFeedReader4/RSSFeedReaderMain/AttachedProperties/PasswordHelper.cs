﻿/*******************************************************************************************************
 * 
 * http://www.wpftutorial.net/PasswordBox.html
 * 
 * 
 ********************************************************************************************************/


using System.Windows;
using System.Windows.Controls;

namespace RSSFeedReader.AttachedProperties
{
    /// <summary>
    /// PasswordHelper class to databind the Password property of PasswordBox.
    /// </summary>
    public static class PasswordHelper
    {

        /// <summary>
        /// The PasswordProperty DependencyProperty
        /// </summary>
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.RegisterAttached("Password",
            typeof(string), typeof(PasswordHelper),
            new FrameworkPropertyMetadata(string.Empty, OnPasswordPropertyChanged));

        /// <summary>
        /// The AttachProperty DependencyProperty
        /// </summary>
        public static readonly DependencyProperty AttachProperty =
            DependencyProperty.RegisterAttached("Attach",
            typeof(bool), typeof(PasswordHelper), new PropertyMetadata(false, Attach));

        /// <summary>
        /// The IsUpdatingProperty DependencyProperty
        /// </summary>
        private static readonly DependencyProperty IsUpdatingProperty =
           DependencyProperty.RegisterAttached("IsUpdating", typeof(bool),
           typeof(PasswordHelper));


        /// <summary>
        /// Sets the attach.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public static void SetAttach( DependencyObject dp, bool value )
        {
            dp.SetValue(AttachProperty, value);
        }

        /// <summary>
        /// Gets the attach.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <returns></returns>
        public static bool GetAttach( DependencyObject dp )
        {
            return (bool)dp.GetValue(AttachProperty);
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <returns>The password</returns>
        public static string GetPassword( DependencyObject dp )
        {
            return (string)dp.GetValue(PasswordProperty);
        }

        /// <summary>
        /// Sets the password.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="value">The value.</param>
        public static void SetPassword( DependencyObject dp, string value )
        {
            dp.SetValue(PasswordProperty, value);
        }

        private static bool GetIsUpdating( DependencyObject dp )
        {
            return (bool)dp.GetValue(IsUpdatingProperty);
        }

        private static void SetIsUpdating( DependencyObject dp, bool value )
        {
            dp.SetValue(IsUpdatingProperty, value);
        }

        private static void OnPasswordPropertyChanged( DependencyObject sender,
            DependencyPropertyChangedEventArgs e )
        {
            PasswordBox passwordBox = sender as PasswordBox;
            if (passwordBox != null)
            {
                passwordBox.PasswordChanged -= PasswordChanged;

                if (!GetIsUpdating(passwordBox))
                {
                    passwordBox.Password = (string)e.NewValue;
                }
                passwordBox.PasswordChanged += PasswordChanged;
            }
        }

        private static void Attach( DependencyObject sender,
            DependencyPropertyChangedEventArgs e )
        {
            PasswordBox passwordBox = sender as PasswordBox;

            if (passwordBox == null)
                return;

            if ((bool)e.OldValue)
            {
                passwordBox.PasswordChanged -= PasswordChanged;
            }

            if ((bool)e.NewValue)
            {
                passwordBox.PasswordChanged += PasswordChanged;
            }
        }

        private static void PasswordChanged( object sender, RoutedEventArgs e )
        {
            var passwordBox = sender as PasswordBox;
            if (passwordBox != null)
            {
                SetIsUpdating(passwordBox, true);
                SetPassword(passwordBox, passwordBox.Password);
                SetIsUpdating(passwordBox, false);
            }
        }
    }
}
