﻿using System;

namespace RSSFeedReader.Dialogs
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveFileDialogMessage : OpenFileDialogMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SaveFileDialogMessage" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="callback">The callback.</param>
        public SaveFileDialogMessage(string content, string filter, Action<bool?, string> callback)
            : base(content, filter, callback)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveFileDialogMessage" /> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="callback">The callback.</param>
        public SaveFileDialogMessage(object sender, string content, string filter, Action<bool?, string> callback)
            : base(sender, content, filter, callback)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveFileDialogMessage" /> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="callback">The callback.</param>
        public SaveFileDialogMessage(object sender, object target, string content, string filter, Action<bool?, string> callback)
            : base(sender, target, content, filter, callback)
        { }
    }
}
