﻿using System;
using System.Diagnostics;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using RSSFeedReader.Resources;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// ViewModel to view and edit the properties of a feed.
    /// </summary>
    public class ChannelPropertiesViewModel : MyViewModelBase
    {
        #region Fields
        readonly ChannelViewModel _channelViewModel;
        #endregion

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.ChannelPropertiesViewModel"/> class.
        /// </summary>
        /// <param name="channelViewModel">The <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/>
        /// instance to use as the data provider.</param>
        public ChannelPropertiesViewModel(ChannelViewModel channelViewModel)
        {
            if (channelViewModel == null)
                throw new ArgumentNullException("channelViewModel");

            _channelViewModel = channelViewModel;
        }

        #endregion

        #region Commands

        #region OKCommand
        private RelayCommand<string> _okCommand;

        /// <summary>
        /// Gets the <see cref="OKCommand"/>.
        /// </summary>
        public RelayCommand<string> OKCommand
        {
            get
            {
                return _okCommand ?? (_okCommand = new RelayCommand<string>(title =>
                    {
                        try
                        {
                            _channelViewModel.Title = title;
                        }
                        catch (Exception ex)
                        {
                            Logger.LogMessage(Strings.LogTypeWarning, string.Format("An error has occured: {0}", ex.Message));
                        }
                        finally
                        {
                            CloseView();
                        }
                    }));
            }
        }
        #endregion

        #region GoToHomePageCommand
        private RelayCommand _goToHomePageCommmand;

        /// <summary>
        /// GoToHomepageCommand to open website homepage
        /// in default web browser.
        /// </summary>
        public RelayCommand GoToHomePageCommand
        {
            get
            {
                return _goToHomePageCommmand ?? (_goToHomePageCommmand = new RelayCommand(() => Process.Start(Link)));
            }
        }
        #endregion

        #region CopyLinkCommand
        private RelayCommand _copyLinkCommand;

        /// <summary>
        /// Gets the <see cref="CopyLinkCommand"/>.
        /// </summary>
        public RelayCommand CopyLinkCommand
        {
            get
            {
                return _copyLinkCommand ?? (_copyLinkCommand = new RelayCommand(
                    () => Clipboard.SetText(Link)));
            }
        }
        #endregion

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the title of the channel.
        /// </summary>
        public string Title
        {
            get { return _channelViewModel.Title; }
        }

        /// <summary>
        /// Gets the Url of the channel.
        /// </summary>
        public string Url
        {
            get { return _channelViewModel.Url; }
        }

        /// <summary>
        /// The Image associated with the feed.
        /// </summary>
        public string ImageUrl
        {
            get { return _channelViewModel.ImageUrl; }
        }

        /// <summary>
        /// Gets the link to the channel.
        /// </summary>
        public string Link
        {
            get { return _channelViewModel.Link; }
        }

        /// <summary>
        /// Gets the summary of the channel.
        /// </summary>
        public string Description
        {
            get { return _channelViewModel.Description; }
        }
        #endregion
    }
}
