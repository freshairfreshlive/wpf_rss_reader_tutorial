﻿using System.Diagnostics;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    ///  Class to hold the proxy settings for the application.
    /// </summary>
    [DebuggerDisplay("{Address}:{Port}")]
    public class ProxySetting
    {
        /// <summary>
        ///The URI of the proxy server.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The port number on host to use.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets if the proxy server is enabled.
        /// </summary>
        public bool IsProxyServerEnabled { get; set; }
    }
}
