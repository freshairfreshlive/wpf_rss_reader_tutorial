﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Represents errors that occur during import/export operations.
    /// </summary>
    public class ImportExportException : Exception
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ImportExportException"/> class.
        /// </summary>
        public ImportExportException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.ImportExportException"/>
        /// class with a specified error message
        /// and the file which is being used.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="file">The file that is associated with the current exception.</param>
        public ImportExportException(string message, string file)
            :base(message)
        {
            File = file;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.ImportExportException"/>
        /// class with a specified error message, a reference to the file which is being 
        /// used and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="file">The file that is associated with the current exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.</param>
        public ImportExportException(string message, string file, Exception innerException)
            : base(message, innerException)
        {
            File = file;
        }

        /// <summary>
        /// Gets the file that is associated with the current <see cref="RSSFeedReader.Data.Models.ImportExportException"/>.
        /// </summary>
        public string File { get; private set; }
    }
}
