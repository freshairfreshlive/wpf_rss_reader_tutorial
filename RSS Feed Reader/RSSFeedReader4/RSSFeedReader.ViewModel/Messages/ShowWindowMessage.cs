﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSSFeedReader.ViewModels.Messages
{
    /// <summary>
    /// Class used to send messages for showing a window/dialog.
    /// </summary>
    /// <typeparam name="TViewModel">The type of the view model.</typeparam>
    public class ShowWindowMessage<TViewModel> where TViewModel : MyViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShowWindowMessage{TViewModel}" /> class.
        /// </summary>
        /// <param name="viewModel">The ViewModel to associate with this message.</param>
        public ShowWindowMessage(TViewModel viewModel)
        {
            ViewModel = viewModel;
        }

        /// <summary>
        /// Gets or sets the view model.
        /// </summary>
        /// <value>
        /// The view model.
        /// </value>
        public TViewModel ViewModel { get; private set; }
    }
}
