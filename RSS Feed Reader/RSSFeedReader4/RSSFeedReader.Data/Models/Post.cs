﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Used to hold data relating to the posts/items
    /// of a <see cref="RSSFeedReader.Data.Models.Channel"/> instance.
    /// </summary>
    [Serializable]
    public class Post : FeedBase
    {
        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.Post"/> class.
        /// </summary>
        public Post()
        {
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.Post"/> class.
        /// </summary>
        /// <param name="title">The title of the Post.</param>
        /// <param name="description">The description of the Post.</param>
        /// <param name="link">The link to the post.</param>
        /// <param name="publishedDate">The date the post was published.</param>
        public Post(string title, string description, string link, string publishedDate)
            : base(title, description, link)
        {
            PublishedDate = publishedDate;
        } 

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the date the post was published.
        /// </summary>
        public string PublishedDate { get; set; }

        /// <summary>
        /// Gets or sets whether the user has read this post.
        /// </summary>
        public bool IsRead { get; set; }

        #endregion
    }
}
