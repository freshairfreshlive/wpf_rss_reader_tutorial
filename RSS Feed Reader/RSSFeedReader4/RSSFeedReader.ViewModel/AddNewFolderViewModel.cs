﻿using System;
using GalaSoft.MvvmLight.Command;
using RSSFeedReader.Data.Models;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// Class to add a new folder to the  <see cref="RSSFeedReader.Data.Models.ChannelDataSource"/>.
    /// </summary>
    public class AddNewFolderViewModel : MyViewModelBase
    {
        #region Fields
        readonly Channel _channel;
        string _name;
        #endregion

        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.AddNewFolderViewModel"/> class.
        /// </summary>
        public AddNewFolderViewModel() { }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.ViewModels.AddNewFolderViewModel"/> class
        /// with a reference to a <see cref="RSSFeedReader.Data.Models.Channel"/> instance.
        /// </summary>
        /// <param name="channel">The <see cref="RSSFeedReader.Data.Models.Channel"/>
        /// instance to use as the data provider.</param>
        public AddNewFolderViewModel(Channel channel)
        {
            if (channel == null)
                throw new ArgumentNullException("channel");

            _channel = channel;
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the name of the folder.
        /// </summary>
        public string FolderName
        {
            get { return _name; }
            set
            {
                if (_name == value)
                    return;

                _name = value;
                RaisePropertyChanged("FolderName");
            }
        }

        #region AddNewFolderCommand
        private RelayCommand _addNewFolderCommand;

        /// <summary>
        /// Adds a new folder.
        /// </summary>
        public RelayCommand AddNewFolderCommand
        {
            get
            {
                return _addNewFolderCommand ?? (_addNewFolderCommand = new RelayCommand(DoAddNewFolderCommand, CanDoAddNewFolderCommand));
            }
        }

        /// <summary>
        /// Determines if the AddNewFolderCommand can be executed.
        /// </summary>
        /// <returns>True if the AddNewFolderCommand can be executed, false otherwise.</returns>
        bool CanDoAddNewFolderCommand()
        {
            return !string.IsNullOrEmpty(FolderName);
        }

        /// <summary>
        /// Executes the AddNewFolderCommand.
        /// </summary>
        void DoAddNewFolderCommand()
        {
            if (_channel != null)
            {
                if (ChannelDataSource.Instance.IsChannelTypeFolder(_channel.ChannelID))
                {
                    ChannelDataSource.Instance.AddNewFolder(_channel.ChannelID, FolderName);
                }
                else if (_channel.ParentID != Guid.Empty)
                {
                    ChannelDataSource.Instance.AddNewFolder(_channel.ParentID, FolderName);
                }
                else
                {
                    ChannelDataSource.Instance.AddNewFolder(FolderName);
                }
            }
            else
            {
                ChannelDataSource.Instance.AddNewFolder(FolderName);
            }

            CloseView();
        }
        #endregion
        #endregion
    }
}
