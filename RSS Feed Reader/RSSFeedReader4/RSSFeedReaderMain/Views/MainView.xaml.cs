﻿/*******************************************************************************************************
 * Code to hide javascript errors
 * http://dave.thehorners.com/tech-talk/programming/389-net-wpf-saying-goodby-to-win32-and-winforms
 * 
 * 
 ********************************************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using RSSFeedReader.Dialogs;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        [ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("6d5140c1-7436-11ce-8034-00aa006009fa")]
        internal interface IServiceProvider
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            object QueryService(ref Guid guidService, ref Guid riid);
        }

        static readonly Guid SidSWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");

        /// <summary>
        /// Initialize a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            Messenger.Default.Register<OpenFileDialogMessage>(this, msg =>
                                                                    {
                                                                        var ofd = new OpenFileDialog { Filter = msg.Filter, Title = msg.Title };
                                                                        var result = ofd.ShowDialog();
                                                                        msg.ProcessCallback(result, ofd.FileName);
                                                                    });
            Messenger.Default.Register<SaveFileDialogMessage>(this, msg =>
                                                                    {
                                                                        var sfd = new SaveFileDialog { Filter = msg.Filter, Title = msg.Title };
                                                                        var result = sfd.ShowDialog();
                                                                        msg.ProcessCallback(result, sfd.FileName);
                                                                    });
            Messenger.Default.Register<DialogMessage>(this, msg =>
                                                            {
                                                                var result = MessageBox.Show(msg.Content, msg.Caption, msg.Button, msg.Icon);
                                                                msg.ProcessCallback(result);
                                                            });
        }

        /// <summary>
        /// Handler for when the browser navigates to a site.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WebBrowserNavigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            MakeComBrowserSilent();
        }

        /// <summary>
        /// Hides javascript errors.
        /// </summary>
        private void MakeComBrowserSilent()
        {
            Guid serviceGuid = SidSWebBrowserApp;
            Guid iid = typeof(SHDocVw.IWebBrowser2).GUID;
            IServiceProvider serviceProvider = (IServiceProvider)webBrowser.Document;
            if (serviceProvider != null)
            {
                SHDocVw.IWebBrowser2 comBrowser = (SHDocVw.IWebBrowser2)serviceProvider.QueryService(ref serviceGuid, ref iid);
                if (comBrowser != null)
                {
                    comBrowser.Silent = true;
                }
            }
        }
    }
}
