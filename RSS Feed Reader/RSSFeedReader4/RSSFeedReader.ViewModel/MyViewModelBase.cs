﻿using System;
using System.Diagnostics;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.Services;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications.
    /// This class is abstract.
    /// </summary>
    public abstract class MyViewModelBase : ViewModelBase
    {
        #region Constructor
        /// <summary>
        /// Initialize a new instance of the MyViewModelBase class.
        /// </summary>
        [DebuggerStepThrough]
        protected MyViewModelBase()
        {
        }
        #endregion

        /// <summary>
        /// Shows the dialog message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="buttons">The buttons.</param>
        /// <param name="icon">The icon.</param>
        /// <param name="callback">The callback.</param>
        protected void ShowDialogMessage(string message, string caption,  MessageBoxButton buttons, MessageBoxImage icon, Action<MessageBoxResult> callback = null)
        {
            var dialog = new DialogMessage(message, callback) { Button = buttons, Caption = caption, Icon = icon };
            Messenger.Default.Send(dialog);
        }

        protected virtual void CloseView()
        {
            Messenger.Default.Send(new CloseWindowMessage());
        }

        #region Services
        /// <summary>
        /// Gets the ILoggerService.
        /// </summary>
        public ILoggerService Logger { get { return Application.Current as ILoggerService; } }
        #endregion
    }
}
