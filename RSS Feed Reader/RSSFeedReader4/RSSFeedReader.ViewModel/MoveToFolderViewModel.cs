﻿using System;
using GalaSoft.MvvmLight.Command;
using RSSFeedReader.Data.Models;

namespace RSSFeedReader.ViewModels
{
    /// <summary>
    /// Allows moving of a <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/> to
    /// <see cref="RSSFeedReader.ViewModels.ChannelViewModel"/> of type
    /// <see cref="RSSFeedReader.Data.Models.ChannelType.Folder"/>.
    /// </summary>
    public class MoveToFolderViewModel : ManageChannelBaseViewModel
    {
        #region Fields
        readonly ChannelViewModel _channelViewModelToMove;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialize a new instance of the 
        /// <see cref="RSSFeedReader.ViewModels.MoveToFolderViewModel"/> class.
        /// </summary>
        public MoveToFolderViewModel(ChannelViewModel channelViewModel)
        {
            if (channelViewModel == null)
                throw new ArgumentNullException("channelViewModel");

            _channelViewModelToMove = channelViewModel;
            Initialize();
        }
        #endregion

        #region Override Properties
        private RelayCommand _okCommand;

        /// <summary>
        /// Override the DoOKCommand.
        /// </summary>
        public override RelayCommand OkCommand
        {
            get
            {
                return _okCommand ?? (_okCommand = new RelayCommand(
                      () =>
                      {
                          ChannelDataSource.Instance.MoveToFolder(_channelViewModelToMove.ChannelID, CurrentChannel.ChannelID);
                          CloseView();
                      }));
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialize data.
        /// </summary>
        void Initialize()
        {
            GetChannelFolders();
        }
        #endregion
    }
}
