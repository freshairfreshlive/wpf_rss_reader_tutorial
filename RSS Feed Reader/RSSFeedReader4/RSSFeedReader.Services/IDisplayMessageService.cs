﻿using System.Windows.Forms;

namespace RSSFeedReader.Services
{
    /// <summary>
    /// Interface to provide a way of showing message dialogues in the Views.
    /// </summary>
    public interface IDisplayMessageService
    {
        /// <summary>
        /// Displays a message box with specified text, caption, buttons, and icon.
        /// </summary>
        /// <param name="message">The text to display in the message box.</param>
        /// <param name="caption">The text to display in the title bar of the message box.</param>
        /// <param name="buttons">One of the System.Windows.Forms.MessageBoxButtons values that specifies which
        /// buttons to display in the message box.</param>
        /// <param name="icon">One of the System.Windows.Forms.MessageBoxIcon values that specifies which
        /// icon to display in the message box.</param>
        /// <returns>One of the System.Windows.Forms.DialogResult values.</returns>
        System.Windows.Forms.DialogResult ShowMessage( string message, string caption,
            MessageBoxButtons buttons, MessageBoxIcon icon );

        /// <summary>
        /// Displays an open file dialoge, with the specified title and filter.
        /// </summary>
        /// <param name="title">The file dialog box title.</param>
        /// <param name="filter">The file filtering options available in the dialog box.</param>
        /// <param name="fileName">The file name selected in the file dialog box.</param>
        /// <returns>One of the System.Windows.Forms.DialogResult values.</returns>
        System.Windows.Forms.DialogResult OpenFileDialog( string title, string filter, out string fileName );

        /// <summary>
        /// Displays a save file dialoge, with the specified title and filter.
        /// </summary>
        /// <param name="title">The file dialog box title.</param>
        /// <param name="filter">The file filtering options available in the dialog box.</param>
        /// <param name="fileName">The file name selected in the file dialog box.</param>
        /// <returns>One of the System.Windows.Forms.DialogResult values.</returns>
        System.Windows.Forms.DialogResult SaveFileDialog( string title, string filter, out string fileName );
    }
}
