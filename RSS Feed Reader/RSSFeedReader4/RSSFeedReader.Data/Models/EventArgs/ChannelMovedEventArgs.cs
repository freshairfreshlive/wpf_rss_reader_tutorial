﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelMoved"/> event.
    /// </summary>
    public class ChannelMovedEventArgs : EventArgs
    {
        #region Constructor

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelMovedToFolderEventArgs"/> class.
        /// </summary>
        /// <param name="channelToMove">The <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        public ChannelMovedEventArgs(Channel channelToMove)
            : this(channelToMove, null)
        {
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelMovedToFolderEventArgs"/> class.
        /// </summary>
        /// <param name="channelToMove">The <see cref="RSSFeedReader.Data.Models.Channel"/> to move.</param>
        /// <param name="channelToMoveTo">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// the channel is being moved to.</param>
        public ChannelMovedEventArgs(Channel channelToMove, Channel channelToMoveTo)
        {
            ChannelToMoveTo = channelToMoveTo;
            ChannelToMove = channelToMove;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the folder the <see cref="RSSFeedReader.Data.Models.Channel"/> is being moved to.
        /// </summary>
        public Channel ChannelToMoveTo { get; private set; }

        /// <summary>
        /// Gets the <see cref="RSSFeedReader.Data.Models.Channel"/> to move.
        /// </summary>
        public Channel ChannelToMove { get; private set; }
        #endregion
    }
}
