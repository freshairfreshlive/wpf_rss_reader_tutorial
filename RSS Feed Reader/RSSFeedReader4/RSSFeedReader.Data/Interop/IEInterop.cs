﻿/*http://community.bartdesmet.net/blogs/bart/archive/2006/11/01/Exploring-the-IE7-RSS-platform-in-C_2300_-_2D00_-Part-1.aspx*/

using System;
using System.Collections.Generic;
using System.Windows;
using Microsoft.Feeds.Interop;
using RSSFeedReader.Data.Models;
using RSSFeedReader.Resources;
using RSSFeedReader.Services;

namespace RSSFeedReader.Data.Interop
{
    /// <summary>
    /// Static class to manage Internet Explorer RSS Feeds.
    /// </summary>
    public static class IEInterop
    {
        static List<Channel> channels;

        /// <summary>
        /// Gets a list of RSS feeds from the Microsoft Feeds API.
        /// </summary>
        /// <returns>Returns a List of type Channel.</returns>
        public static List<Channel> GetIERSSFeeds()
        {
            IEInterop.channels = new List<Channel>();
            FeedsManager feedsManager = new FeedsManagerClass();
            IFeedFolder root = (IFeedFolder)feedsManager.RootFolder;

            GetFeeds(root, null, 0);

            return IEInterop.channels;
        }

        /// <summary>
        /// Gets the feeds subscribed to in IE.
        /// </summary>
        /// <param name="parent">The parent folder to look in.</param>
        /// <param name="parentChannel">Parent channel to add feeds to.</param>
        /// <param name="n"></param>
        private static void GetFeeds(IFeedFolder parent, Channel parentChannel, int n)
        {
            Channel folderChannel = null;
            Channel feedChannel = null;

            if (n > 0)
            {
                if (parentChannel != null)
                {
                    folderChannel =
                        new Channel(Guid.NewGuid(), parentChannel, parent.Name, null, null, null, null);
                    folderChannel.ChannelType = ChannelType.Folder;
                    parentChannel.Children.Add(folderChannel);
                    folderChannel.ParentID = parentChannel.ChannelID;
                }
                else
                {
                    folderChannel = new Channel(Guid.NewGuid(), parent.Name, null, null, null, null);
                    folderChannel.ChannelType = ChannelType.Folder;
                }
            }

            foreach (IFeed feed in (IFeedsEnum)parent.Feeds)
            {
                try
                {
                    if (parentChannel == null)
                    {
                        feedChannel =
                            new Channel(
                                Guid.NewGuid(), feed.Name, null, feed.Url, null, feed.Url);
                    }
                    else
                    {
                        feedChannel =
                            new Channel(
                                Guid.NewGuid(), feed.Name, null, feed.Url, null, feed.Url);
                    }

                    if (folderChannel != null)
                    {
                        folderChannel.Children.Add(feedChannel);
                        feedChannel.ParentID = folderChannel.ChannelID;
                    }
                }
                catch (Exception e)
                {
                    ILoggerService logger = Application.Current as ILoggerService;
                    logger.LogMessage(Strings.LogTypeWarning, "Error getting Channel from IE import." + e.Message );
                }
            }

            if (parentChannel == null)
            {
                if (folderChannel != null)
                {
                    IEInterop.channels.Add(folderChannel);
                }
                else if (feedChannel != null)
                {
                    IEInterop.channels.Add(feedChannel);
                }
            }

            foreach (IFeedFolder folder in (IFeedsEnum)parent.Subfolders)
            {
                GetFeeds(folder, folderChannel, n + 1);
            }
        }
    }
}
