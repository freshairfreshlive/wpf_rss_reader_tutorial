﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Represents errors that occur whilst trying to download a feed.
    /// </summary>
    public class FeedLoadException : Exception
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedLoadException"/> class with a specified
        ///  error message.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        public FeedLoadException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedLoadException"/> class with a specified
        /// error message and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.
        /// </param>
        public FeedLoadException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RSSFeedReader.Data.Models.FeedLoadException"/> class with a specified
        /// error message, the url that failed to load
        /// and a reference to the inner exception that is the cause of
        /// this exception.
        /// </summary>
        /// <param name="message">
        /// The message that describes the error.
        /// </param>
        /// <param name="url">
        /// The url of the feed that failed to load.
        /// </param>
        /// <param name="innerException">
        /// The exception that is the cause of the current exception, or a null reference
        /// if no inner exception is specified.
        /// </param>
        public FeedLoadException(string message, string url, Exception innerException)
            : base(message, innerException)
        {
            ChannelUrl = url;
        }

        /// <summary>
        /// Gets the url that failed to load.
        /// </summary>
        public string ChannelUrl { get; private set; }
    }
}
