﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for MoveToFolderView.xaml
    /// </summary>
    public partial class MoveToFolderView : Window
    {
        /// <summary>
        /// Initialize a new instance of the 
        /// <see cref="RSSFeedReader.Views.MoveToFolderView"/> class.
        /// </summary>
        public MoveToFolderView()
        {
            InitializeComponent(); 
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
