﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using RSSFeedReader.ViewModels.Messages;

namespace RSSFeedReader.Views
{
    /// <summary>
    /// Interaction logic for RenameChannelView.xaml
    /// </summary>
    public partial class RenameChannelView : Window
    {
        /// <summary>
        /// Initialize a new instance of the <see cref="RenameChannelView"/> class.
        /// </summary>
        public RenameChannelView()
        {
            InitializeComponent();
            _NameTextBox.Focus();
            Messenger.Default.Register<CloseWindowMessage>(this, message => Close());
        }
    }
}
