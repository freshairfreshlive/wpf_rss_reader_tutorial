﻿using System;

namespace RSSFeedReader.Data.Models
{
    /// <summary>
    /// Provides data for the <see cref="RSSFeedReader.Data.Models.ChannelDataSource.ChannelFolderAdded"/> event.
    /// </summary>
    public class ChannelFolderAddedEventArgs : EventArgs
    {
        #region Constructor
        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelFolderAddedEventArgs"/> class.
        /// </summary>
        /// <param name="newFolder">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// folder being added.</param>
        public ChannelFolderAddedEventArgs(Channel newFolder)
        {
            NewFolder = newFolder;
        }

        /// <summary>
        /// Initialize a new instance of the <see cref="RSSFeedReader.Data.Models.ChannelFolderAddedEventArgs"/> class.
        /// </summary>
        /// <param name="parent">The parent of this <see cref="RSSFeedReader.Data.Models.Channel"/>.</param>
        /// <param name="newFolder">The <see cref="RSSFeedReader.Data.Models.Channel"/> 
        /// folder being added.</param>
        public ChannelFolderAddedEventArgs(Channel parent, Channel newFolder)
        {
            Parent = parent;
            NewFolder = newFolder;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the new folder <see cref="RSSFeedReader.Data.Models.Channel"/> instance.
        /// </summary>
        public Channel NewFolder { get; private set; }

        /// <summary>
        /// Gets the parent of this <see cref="RSSFeedReader.Data.Models.Channel"/>.
        /// </summary>
        public Channel Parent { get; private set; }
        #endregion
    }
}
